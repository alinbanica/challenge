class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name
      t.string :country
      t.string :gender
      t.integer :age
      t.belongs_to :user, index: true

      t.timestamps
    end
  end
end
