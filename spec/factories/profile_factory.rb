FactoryGirl.define do
  factory :profile do
    sequence(:name) { |n| "User#{n}" }
    gender { Profile::GENDER_LIST.keys.first }
    age { 40 }
    country { 'Romania' }
  end
end
