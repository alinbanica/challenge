require 'rails_helper'

describe User, type: :model do
  describe 'fields' do
    it { is_expected.to respond_to(:email) }
  end

  describe 'Validations' do
    let!(:user) { FactoryGirl.build(:user) }

    context 'not_valid' do
      it_behaves_like 'empty_field', 'email', :user do
        let(:resource) { user }
      end

      it "expect user object not be valid if email is not correct" do
        user['email'] = 'dddd'
        expect(user).to_not be_valid
      end
    end

    context 'valid' do
      it 'expect user object to be valid' do
        expect(user).to be_valid
      end
    end
  end

  describe 'instance methods' do
    let!(:user) { FactoryGirl.create(:user) }

    describe '#name' do
      it 'show the email of the user if profile is not defined' do
        expect(user.name).to eq(user.email)
      end

      it 'show the name of the user if profile exists' do
        FactoryGirl.create(:profile, user: user)
        expect(user.name).to eq(user.profile.name)
      end
    end

    describe '#my_project?' do
      it 'expect to be true' do
        project = FactoryGirl.create(:project, user: user)
        expect(user.my_project?(project.id)).to be true
      end

      it 'expect to be false' do
        project = FactoryGirl.create(:project)
        expect(user.my_project?(project.id)).to be false
      end
    end

    describe '#liked_project?' do
      let!(:project) { FactoryGirl.create(:project) }

      it 'expect to be true if the project already liked' do
        like = FactoryGirl.create(:like, user: user, project: project)
        expect(user.liked_project?(project.id)).to be true
      end

      it 'expect to be false if the project not liked' do
        expect(user.liked_project?(project.id)).to be false
      end
    end

    describe '#my_like?' do
      let!(:project) { FactoryGirl.create(:project) }

      it 'expect to be true if the like is mine' do
        like = FactoryGirl.create(:like, user: user, project: project)
        expect(user.my_like?(like.id)).to be true
      end

      it 'expect to be false if the like is not mine' do
        like = FactoryGirl.create(:like, project: project)
        expect(user.my_like?(like.id)).to be false
      end
    end

    describe '#following?' do
      it 'expect to be true if the user followes another user' do
        follower = FactoryGirl.create(:follower, follower: user)
        expect(user.following?follower.followed_id).to be true
      end

      it 'expect to be false if the user followes another user' do
        follower = FactoryGirl.create(:follower)
        expect(user.following?follower.followed_id).to be false
      end
    end
  end
end
