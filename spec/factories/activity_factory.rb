FactoryGirl.define do
  factory :activity do
    user
    action 'some_action'
    message 'this is an activity message'
  end
end
