class Project < ActiveRecord::Base
  belongs_to :user
  has_many :likes

  scope :for_user, -> (user_id) { where(user_id: user_id) }
  scope :not_for_user, -> (user_id) {  where.not(user_id: user_id) }

  validates :name, :user_id, presence: true
end
