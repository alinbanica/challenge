class Profile < ActiveRecord::Base
  GENDER_LIST = {
    'male' => 'Male',
    'female' => 'Female'
  }.freeze

  belongs_to :user

  validates :name, :gender, :age, :country, presence: true
  validates :age, numericality: { only_integer: true, greater_than: 0 }
  validates :gender, inclusion: { in: GENDER_LIST.keys }
  validates :country, inclusion: { in: ISO3166::Country.all.map(&:name) }
end
