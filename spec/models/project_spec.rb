require 'rails_helper'

describe Project, type: :model do
  describe 'fields' do
    it { is_expected.to respond_to(:name) }
  end

  describe 'Validations' do
    let!(:project) { FactoryGirl.build(:project) }

    context 'not_valid' do
      it_behaves_like 'empty_field', 'name', :project do
        let(:resource) { project }
      end
    end

    context 'valid' do
      it 'expect project object to be valid' do
        expect(project).to be_valid
      end
    end
  end
end
