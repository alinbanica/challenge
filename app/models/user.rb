class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :profile
  has_many :projects

  has_many :following_users, class_name: 'Follower', foreign_key: 'followed_id'
  has_many :followers, through: :following_users

  has_many :followed_users, class_name: 'Follower', foreign_key: 'user_id'
  has_many :followings, through: :followed_users

  has_many :likes, class_name: 'Like', foreign_key: 'user_id'
  has_many :liked_projects, through: :likes, source: :project

  has_many :activities

  accepts_nested_attributes_for :profile, allow_destroy: true

  def name
    profile ? profile.name : email
  end

  def following?(user_id)
    followings.where(id: user_id).present?
  end

  def my_like?(like_id)
    likes.find{ |like| like.id == like_id }.present?
  end

  def liked_project?(project_id)
    liked_projects.where(id: project_id).present?
  end

  def my_project?(project_id)
    projects.find{ |project| project.id == project_id }.present?
  end
end
