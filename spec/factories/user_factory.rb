FactoryGirl.define do
  factory :user do
    sequence(:email) { |n| "user#{n}@example.com" }
    password { 'parola1234' }
  end

  factory :user_with_profile, parent: :user do
    after(:create) do |user|
      create(:profile)
    end
  end
end
