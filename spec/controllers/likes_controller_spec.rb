require 'rails_helper'

describe LikesController, type: :controller do
  include Devise::TestHelpers

  let!(:user) { FactoryGirl.create(:user) }
  let!(:project) { FactoryGirl.create(:project, user: user) }
  let!(:like) { FactoryGirl.create(:like, user: user) }

  describe 'create' do
    let!(:options){
      {
        project_id: project.id
      }
    }

    subject { post :create, options }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should redirect to user projects page' do
        subject
        expect(response).to redirect_to(projects_users_path)
      end

      it 'should log activity' do
        expect{ subject }.to change { Activity.where(action: 'like_project').count }.by(1)
      end

      it 'should create a like' do
        expect{ subject }.to change { Like.count }.by(1)
        expect(flash[:success]).to_not be_empty
      end

      context 'not create like' do
        it_behaves_like 'validate like params'
      end
    end
  end

  describe 'destroy' do
    let!(:options){
      {
        id: like.id
      }
    }

    subject { delete :destroy, options }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should redirect to user projects page' do
        subject
        expect(response).to redirect_to(other_projects_path)
      end

      it 'should log activity' do
        expect{ subject }.to change { Activity.where(action: 'unlike_project').count }.by(1)
      end

      it 'should unlike a project' do
        expect{ subject }.to change { Like.count }.by(-1)
        expect(flash[:success]).to_not be_empty
      end

      it 'should not destroy a follower from another user' do
        like2 = FactoryGirl.create(:like)
        expect{ delete :destroy, id: like2.id}.to_not change { Like.count }
      end
    end
  end
end
