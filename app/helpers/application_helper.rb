module ApplicationHelper
  def global_message(key, value)
    content_tag(:div, close_button(value), :class => "alert alert-#{key}", role: "alert")
  end

  def projects_submit_button_texf(method)
    case method
    when :post
      'Create project'
    when :put
      'Update project'
    else
      ''
    end
  end

  def show_followers_link(followed_id)
    if current_user.following?(followed_id)
      follower = current_user.followed_users.where(followed_id: followed_id).last
      content = link_to('Unfollow', follower_path(follower), method: :delete, class: 'unfollow')
    else
      content = link_to('Follow', followers_path(followed_id: followed_id), method: :post, class: 'follow')
    end
  end

  def close_button(message)
    content = button_tag('x', class: 'close', data: { dismiss: 'alert' })
    content += message
  end

  def show_like_link(project_id)
    if current_user.liked_project?(project_id)
      like = current_user.likes.where(project_id: project_id).last
      content = link_to('Unlike', like_path(like), method: :delete, class: 'unlike')
    else
      content = link_to('Like', likes_path(project_id: project_id), method: :post, class: 'like')
    end
  end

  def show_date(date_time)
    date_time.strftime("%m/%d/%Y %I:%M %p")
  end
end
