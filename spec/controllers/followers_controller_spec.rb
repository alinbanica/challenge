require 'rails_helper'

describe FollowersController, type: :controller do
  include Devise::TestHelpers

  let!(:user) { FactoryGirl.create(:user) }
  let!(:user2) { FactoryGirl.create(:user) }
  let!(:follower) { FactoryGirl.create(:follower, follower: user) }

  describe 'create' do
    let!(:options){
      {
        followed_id: user2.id
      }
    }

    subject { post :create, options }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should redirect to other users page' do
        subject
        expect(response).to redirect_to(other_users_path)
      end

      it 'should log activity' do
        expect{ subject }.to change { Activity.where(action: 'follow_user').count }.by(1)
      end

      it 'should create a like' do
        expect{ subject }.to change { Follower.count }.by(1)
        expect(flash[:success]).to_not be_empty
      end

      context 'not create the follower' do
        it_behaves_like 'validate follower params'
      end
    end
  end

  describe 'destroy' do
    let!(:options){
      {
        id: follower.id
      }
    }

    subject { delete :destroy, options }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should redirect to other users page' do
        subject
        expect(response).to redirect_to(other_users_path)
      end

      it 'should log activity' do
        expect{ subject }.to change { Activity.where(action: 'unfollow_user').count }.by(1)
      end

      it 'should unlike a project' do
        expect{ subject }.to change { Follower.count }.by(-1)
        expect(flash[:success]).to_not be_empty
      end

      it 'should not destroy a follower from another user' do
        follower2 = FactoryGirl.create(:follower)
        expect{ delete :destroy, id: follower2.id}.to_not change { Follower.count }
      end
    end
  end
end
