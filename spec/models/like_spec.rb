require 'rails_helper'

describe Like, type: :model do
  describe 'fields' do
    it { is_expected.to respond_to(:project_id) }
    it { is_expected.to respond_to(:user_id) }
  end

  describe 'Validations' do
    let!(:like) { FactoryGirl.build(:like) }

    context 'not_valid' do
      it_behaves_like 'empty_field', 'user_id', :like do
        let(:resource) { like }
      end

      it_behaves_like 'empty_field', 'project_id', :like do
        let(:resource) { like }
      end

      it 'expect like object to not be valid if already exists a combination of user and project' do
        like.save!
        like2 = FactoryGirl.build(:like, user_id: like.user_id, project_id: like.project_id)
        expect(like2).to_not be_valid
      end
    end

    context 'valid' do
      it 'expect like object to be valid' do
        expect(like).to be_valid
      end
    end
  end
end
