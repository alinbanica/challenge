class CreateFollowers < ActiveRecord::Migration
  def change
    create_table :followers do |t|
      t.integer :user_id, index: true
      t.integer :followed_id, index: true

      t.timestamps
    end

    add_index :followers, [:user_id, :followed_id], unique: true
  end
end
