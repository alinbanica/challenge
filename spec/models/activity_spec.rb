require 'rails_helper'

describe Activity, type: :model do
  describe 'fields' do
    it { is_expected.to respond_to(:user_id) }
    it { is_expected.to respond_to(:action) }
    it { is_expected.to respond_to(:message) }
  end

  describe 'Validations' do
    let!(:activity) { FactoryGirl.build(:activity) }

    context 'not_valid' do
      it_behaves_like 'empty_field', 'user_id', :activity do
        let(:resource) { activity }
      end

      it_behaves_like 'empty_field', 'action', :activity do
        let(:resource) { activity }
      end

      it_behaves_like 'empty_field', 'message', :activity do
        let(:resource) { activity }
      end
    end

    context 'valid' do
      it 'expect activity object to be valid' do
        expect(activity).to be_valid
      end
    end
  end

  describe 'class methods' do
    let!(:user) { FactoryGirl.create(:user_with_profile) }

    describe '.log' do
      it 'expect to add new log' do
        options = {
          user: user,
          message: 'Some message',
          action: 'test'
        }

        expect{ Activity.log(options) }.to change { Activity.count }.by(1)
      end
    end
  end
end
