class UsersController < ApplicationController
  def projects
    @projects = Project.for_user(current_user.id)
    @projects = @projects.page(params[:page]).per_page(PAGE_SIZE)
  end

  def other
    @users = User.where.not(id: current_user.id)
    @users = @users.page(params[:page]).per_page(PAGE_SIZE)
  end

  def notifications
    @activities = Activity.for_user(current_user.followings.map(&:id))
                    .order('created_at desc')
    @activities = @activities.page(params[:page]).per_page(PAGE_SIZE)
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    @user.update_attributes(users_params)

    if @user.valid?
      flash[:success] = 'The profile data was updated successfully!'
      redirect_to root_path
    else
      flash[:error] = join_errors(@user)
      render 'edit'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  private

  def users_params
    params.require(:user).permit(:password, profile_attributes: [:id, :name, :gender, :age, :country])
  end
end
