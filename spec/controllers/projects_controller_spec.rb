require 'rails_helper'

describe ProjectsController, type: :controller do
  include Devise::TestHelpers

  let!(:user) { FactoryGirl.create(:user) }
  let!(:project) { FactoryGirl.create(:project, user: user) }

  describe 'new' do
    subject { get :new }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should render edit page' do
        subject
        expect(response).to render_template(:new)
      end
    end
  end

  describe 'edit' do
    subject { get :edit, id: project.id }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should render edit page' do
        subject
        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'show' do
    subject { get :show, id: project.id }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    describe 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should render show page' do
        subject
        expect(response).to render_template(:show)
      end
    end
  end

  describe 'update' do
    let!(:options){
      {
        id: project.id,
        project:{
          name: 'Haha production'
        }
      }
    }

    subject { put :update, options }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should redirect to user projects page' do
        expect(subject).to redirect_to(projects_users_path)
      end

      it 'should log activity' do
        expect{ subject }.to change { Activity.count }.by(1)
      end

      it 'should update the project data' do
        subject
        project.reload
        expect(project.name).to eq(options[:project][:name])
        expect(flash[:success]).to_not be_empty
      end

      context 'not updating the project' do
        it_behaves_like 'validate project params', 'invalid_data_on_update'
      end
    end
  end

  describe 'create' do
    let!(:options){
      {
        project:{
          name: 'Haha production'
        }
      }
    }

    subject { post :create, options }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should redirect to user projects page' do
        subject
        expect(response).to redirect_to(projects_users_path)
      end

      it 'should log activity' do
        expect{ subject }.to change { Activity.count }.by(1)
      end

      it 'should create the project' do
        expect{ subject }.to change { Project.count }.by(1)
        expect(flash[:success]).to_not be_empty
      end

      context 'not create the project' do
        it_behaves_like 'validate project params', 'invalid_data_on_create'
      end
    end
  end

  describe 'other' do
    subject { get :other }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should return only the projects that are from other users' do
        project2 = FactoryGirl.create(:project)

        subject
        expect(assigns(:projects).map(&:id)).to eq([project2.id].sort)
      end

      it 'should return no projects' do
        subject
        expect(assigns(:projects)).to be_empty
      end
    end
  end
end
