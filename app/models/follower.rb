class Follower < ActiveRecord::Base
  belongs_to :follower, class_name: 'User', foreign_key: 'user_id'
  belongs_to :following, class_name: 'User', foreign_key: 'followed_id'

  validates :user_id, :followed_id, presence: true
  validates :user_id, uniqueness: { scope: :followed_id }

  validate :follow_me

  def follow_me
    errors.add(:base, 'You can not follow you') if self.user_id == self.followed_id
  end
end
