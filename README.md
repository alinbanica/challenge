## Problem explanation

In our system users can follow other users, create and like projects.

We want to build a notification system where when a user A is following
a user B, we want to notify the former with the activities taken by the
latter.

For example, suppose Alice is following Bob. When Bob follows Tom;
creates and likes a project, Alice expects to see the following
notifications:

    * Bob started following Tom
    * Bob created the project "In the Wonderland"
    * Bob liked the project "In the Wonderland"

## General notes

As you work on your solutions, you should make commits to the repository
along the way, just as you would on a 'real' project. This is so we can
see the progression of how you came to the solutions you did.

This folder already contains an empty git repository.

Have fun, and feel free to ask questions if anything is unclear.
