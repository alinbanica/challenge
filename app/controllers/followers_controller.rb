class FollowersController < ApplicationController
  def create
    request_response = { data: {}, errors: [], meta: {} }.with_indifferent_access

    params[:user_id] = current_user.id

    @follower = Follower.new(followers_params)

    if @follower.save
      Activity.log({
        user_id: current_user.id,
        action: 'follow_user',
        message: "#{current_user.name} start following #{@follower.following.name}"
      })

      flash[:success] = "You are now following #{@follower.following.name}"
    else
      flash[:error] = join_errors(@follower)
    end

    redirect_to other_users_path
  end

  def destroy
    request_response = { data: {}, errors: [], meta: {} }.with_indifferent_access

    @follower = Follower.find(params[:id])

    if current_user.following?(@follower.followed_id) && @follower.destroy
      Activity.log({
        user_id: current_user.id,
        action: 'unfollow_user',
        message: "#{current_user.name} stop following #{@follower.following.name}"
      })

      flash[:success] = "You stop following #{@follower.following.name}"
    else
      flash[:error] = "Somethning went wrong"
    end

    redirect_to other_users_path
  end

  private

  def followers_params
    params.permit(:user_id, :followed_id)
  end
end
