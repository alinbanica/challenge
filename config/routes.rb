Rails.application.routes.draw do
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords'
  }

  root to: "users#notifications"

  resources :users, only:[:show, :edit, :update] do
    collection do
      get 'other'
      get 'projects'
      get 'notifications'
    end
  end

  resources :projects do
    collection do
      get 'other'
    end
  end

  resources :followers, only: [:create, :destroy]
  resources :likes, only: [:create, :destroy]
end
