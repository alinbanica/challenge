require 'rails_helper'

describe Follower, type: :model do
  describe 'fields' do
    it { is_expected.to respond_to(:user_id) }
    it { is_expected.to respond_to(:followed_id) }
  end

  describe 'Validations' do
    let!(:follower) { FactoryGirl.build(:follower) }

    context 'not_valid' do
      it_behaves_like 'empty_field', 'user_id', :follower do
        let(:resource) { follower }
      end

      it_behaves_like 'empty_field', 'followed_id', :follower do
        let(:resource) { follower }
      end

      it 'expect object follower to not be valid if a user is already followed' do
        follower.save!
        follower2 = FactoryGirl.build(:follower, user_id: follower.user_id,
          followed_id: follower.followed_id)
        expect(follower2).to_not be_valid
      end

      it 'expect object follower to not be valid if user and followed are the same' do
        follower.followed_id = follower.user_id
        expect(follower).to_not be_valid
      end
    end

    context 'valid' do
      it 'expect follower object to be valid' do
        expect(follower).to be_valid
      end
    end
  end
end
