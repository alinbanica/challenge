require 'rails_helper'

describe UsersController, type: :controller do
  include Devise::TestHelpers

  let!(:user) { FactoryGirl.create(:user) }

  describe 'projects' do
    subject { get :projects, id: user.id }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should render projects page' do
        get :projects, id: user.id
        expect(response).to render_template(:projects)
      end

      it 'should return no projects' do
        get :projects, id: user.id
        expect(assigns(:projects)).to be_empty
      end

      it 'should return only user projects' do
        project, project2 = FactoryGirl.create_list(:project, 2, user: user)
        project3 = FactoryGirl.create(:project)

        get :projects, id: user.id
        expect(assigns(:projects).map(&:id).sort).to eq([project.id, project2.id].sort)
      end
    end
  end

  describe 'other' do
    subject { get :other }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should render other users page' do
        subject
        expect(response).to render_template(:other)
      end

      it 'should return no users' do
        subject
        expect(assigns(:users)).to be_empty
      end

      it 'should return all other users' do
        user3, user2 = FactoryGirl.create_list(:user, 2)

        subject
        expect(assigns(:users).map(&:id).sort).to eq([user3.id, user2.id].sort)
      end
    end
  end

  describe 'notifications' do
    subject { get :notifications }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should render notifications page' do
        subject
        expect(response).to render_template(:notifications)
      end

      it 'should return no notifications' do
        user2 = FactoryGirl.create(:user)
        notification = FactoryGirl.create(:activity,
          user: user2,
          action: 'create_project',
          message: 'A new project was created')

        subject
        expect(assigns(:activities)).to be_empty
      end

      it 'should return only followed users notifications' do
        user2 = FactoryGirl.create(:user)
        FactoryGirl.create(:follower, user_id: user.id, followed_id: user2.id)

        notification = FactoryGirl.create(:activity,
          user: user2,
          action: 'create_project',
          message: 'A new project was created')

        notification2 = FactoryGirl.create(:activity,
          action: 'like_project',
          message: 'Project x liked')

        notification3 = FactoryGirl.create(:activity,
          user: user2,
          action: 'update_project',
          message: 'A project was updated')

        subject
        expect(assigns(:activities).map(&:id).sort).to eq([notification3.id, notification.id].sort)
      end
    end
  end

  describe 'edit' do
    subject { get :edit, id: user.id }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should render edit page' do
        subject
        expect(response).to render_template(:edit)
      end
    end
  end

  describe 'show' do
    subject { get :show, id: user.id }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    describe 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should render show page' do
        subject
        expect(response).to render_template(:show)
      end
    end
  end

  describe 'update' do
    let!(:options){
      {
        id: user.id,
        user:{
          profile_attributes: {
            name: 'Daniel',
            country: 'Romania',
            age: 23,
            gender: 'male'
          }
        }
      }
    }

    subject { put :update, options }

    context 'not authenticated' do
      it_behaves_like 'not_authenticated'
    end

    context 'authenticated' do
      before :each do
        sign_in user
      end

      it 'should redirect to dashboard' do
        put :update, options
        expect(response).to redirect_to(root_path)
      end

      it 'should update the user profile data' do
        put :update, options

        user.reload
        expect(user.profile.name).to eq(options[:user][:profile_attributes][:name])
        expect(user.profile.age).to eq(options[:user][:profile_attributes][:age])
        expect(user.profile.country).to eq(options[:user][:profile_attributes][:country])
        expect(user.profile.gender).to eq(options[:user][:profile_attributes][:gender])
        expect(flash[:success]).to_not be_empty
      end

      context 'not updating the user profile' do
        it_behaves_like 'validate user params'
      end
    end
  end
end
