class ProjectsController < ApplicationController
  def other
    @projects = Project.not_for_user(current_user.id)
    @projects = @projects.page(params[:page]).per_page(PAGE_SIZE)
  end

  def new
    @project = Project.new
  end

  def create
    params[:project][:user_id] = current_user.id

    @project = Project.new(project_params)

    if @project.save
      Activity.log({
        user_id: current_user.id,
        action: 'create_project',
        message: "#{current_user.name} created the project #{@project.name}"
      })

      flash[:success] = 'The new project was saved successfully!'
      redirect_to projects_users_path
    else
      flash[:error] = join_errors(@project)
      render 'new'
    end
  end

  def edit
    @project = Project.find(params[:id])
  end

  def update
    @project = Project.find(params[:id])
    @project.update_attributes(project_params)

    if @project.valid?
      Activity.log({
        user_id: current_user.id,
        action: 'update_project',
        message: "#{current_user.name} updatec the project #{@project.name}"
      })

      flash[:success] = 'The project was updated successfully!'
      redirect_to projects_users_path
    else
      flash[:error] = join_errors(@project)
      render 'edit'
    end
  end

  def show
    @project = Project.find(params[:id])
  end

  private

  def project_params
    params.require(:project).permit(:name, :user_id)
  end
end
