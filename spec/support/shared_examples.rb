module SharedExamples
  shared_examples_for 'not_authenticated' do
    it 'should be redirected to sign in page' do
      subject
      expect(response).to redirect_to(new_user_session_path)
    end
  end

  shared_examples_for 'empty_field' do |field, obj_class|
    it "expect #{obj_class} object not be valid if #{field} empty" do
      resource[field] = ''
      expect(resource).to_not be_valid
    end
  end

  shared_examples 'invalid_data_on_update' do |text|
    it "should not update the data if #{text}" do
      sign_in user
      put :update, opts

      expect(flash[:error]).to_not be_empty
      expect(response).to render_template('edit')
    end
  end

  shared_examples 'invalid_data_on_create' do |text|
    it "should not update the data if #{text}" do
      sign_in user
      post :create, opts

      expect(flash[:error]).to_not be_empty
      expect(response).to render_template('new')
    end
  end

  shared_examples 'validate user params' do
    it_behaves_like 'invalid_data_on_update', 'missing name' do
      let!(:opts) {
        options[:user][:profile_attributes].except!(:name)
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'missing country' do
      let!(:opts) {
        options[:user][:profile_attributes].except!(:country)
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'missing age' do
      let!(:opts) {
        options[:user][:profile_attributes].except!(:age)
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'missing gender' do
      let!(:opts) {
        options[:user][:profile_attributes].except!(:gender)
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'empty name' do
      let!(:opts) {
        options[:user][:profile_attributes][:name] = ''
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'empty country' do
      let!(:opts) {
        options[:user][:profile_attributes][:country] = ''
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'empty age' do
      let!(:opts) {
        options[:user][:profile_attributes][:age] = ''
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'empty gender' do
      let!(:opts) {
        options[:user][:profile_attributes][:gender] = ''
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'invalid age' do
      let!(:opts) {
        options[:user][:profile_attributes][:age] = 'eeee'
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'age is less than 0' do
      let!(:opts) {
        options[:user][:profile_attributes][:age] = -10
        options
      }
    end

    it_behaves_like 'invalid_data_on_update', 'invalid gender' do
      let!(:opts) {
        options[:user][:profile_attributes][:gender] = 'dfssd'
        options
      }
    end
  end

  shared_examples 'validate project params' do |example|
    it_behaves_like example, 'empty name' do
      let!(:opts) {
        options[:project][:name] = ''
        options
      }
    end
  end

  shared_examples 'invalid_data_on_create_with_redirect' do |text|
    it "should not update the data if #{text}" do
      sign_in user
      post :create, opts

      expect(flash[:error]).to_not be_empty
      expect(response).to redirect_to(redirect_path)
    end
  end

  shared_examples 'validate like params' do |example|
    it_behaves_like 'invalid_data_on_create_with_redirect', 'empty project_id' do
      let!(:opts) {
        options[:project_id] = ''
        options
      }
      let!(:redirect_path) { other_projects_path }
    end
  end

  shared_examples 'validate follower params' do |example|
    it_behaves_like 'invalid_data_on_create_with_redirect', 'empty project_id' do
      let!(:opts) {
        options[:followed_id] = ''
        options
      }
      let!(:redirect_path) { other_users_path }
    end
  end
end
