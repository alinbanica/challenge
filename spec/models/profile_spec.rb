require 'rails_helper'

describe Profile, type: :model do
  describe 'fields' do
    it { is_expected.to respond_to(:name) }
    it { is_expected.to respond_to(:country) }
    it { is_expected.to respond_to(:gender) }
    it { is_expected.to respond_to(:age) }
  end

  describe 'Validations' do
    let!(:profile) { FactoryGirl.build(:profile) }

    context 'not_valid' do
      context 'empty fields' do
        it_behaves_like 'empty_field', 'name', :profile do
          let(:resource) { profile }
        end

        it_behaves_like 'empty_field', 'country', :profile do
          let(:resource) { profile }
        end

        it_behaves_like 'empty_field', 'gender', :profile do
          let(:resource) { profile }
        end

        it_behaves_like 'empty_field', 'age', :profile do
          let(:resource) { profile }
        end
      end

      context 'age validations' do
        it 'expect object profile to not be valid if age is string' do
          profile['age'] = 'ddd'
          expect(profile).to_not be_valid
        end

        it 'expect object profile to not be valid if age is less then 0' do
          profile['age'] = -20
          expect(profile).to_not be_valid
        end
      end

      it 'expect object profile to not be valid if gender is not male or female' do
        profile['gender'] = 'other'
        expect(profile).to_not be_valid
      end

      it 'expect object profile to not be valid if country is not in the list' do
        profile['country'] = 'other'
        expect(profile).to_not be_valid
      end
    end

    context 'valid' do
      it 'expect profile object to be valid' do
        expect(profile).to be_valid
      end
    end
  end
end
