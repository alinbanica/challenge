class LikesController < ApplicationController
   def create
    request_response = { data: {}, errors: [], meta: {} }.with_indifferent_access

    params[:user_id] = current_user.id

    @like = Like.new(likes_params)

    if @like.save
      Activity.log({
        user_id: current_user.id,
        action: 'like_project',
        message: "#{current_user.name} like project #{@like.project.name}"
      })

      flash[:success] = "You liked project '#{@like.project.name}'."
    else
      flash[:error] = join_errors(@like)
    end

    redirect_to redirect_project_path
  end

  def destroy
    request_response = { data: {}, errors: [], meta: {} }.with_indifferent_access

    @like = Like.find(params[:id])

    if current_user.my_like?(@like.id) && @like.destroy
      Activity.log({
        user_id: current_user.id,
        action: 'unlike_project',
        message: "#{current_user.name} unlike project #{@like.project.name}"
      })

      flash[:success] = "You unliked project '#{@like.project.name}'."
    else
      flash[:error] = "Something went wrong."
    end

    redirect_to redirect_project_path
  end

  private

  def likes_params
    params.permit(:user_id, :project_id)
  end

  def redirect_project_path
    current_user.my_project?(@like.project_id) ? projects_users_path : other_projects_path
  end
end
