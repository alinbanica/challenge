class Activity < ActiveRecord::Base
  belongs_to :user

  validates :user_id, :action, :message, presence: true

  scope :for_user, -> (user_id) { where(user_id: user_id) }

  def self.log(options)
    activity = Activity.new(options)
    activity.save
  end
end
